<?php

namespace Services\PaymentStrategy\Contacts;
use Contracts\OrderInterface;

interface PaymentsStrategy
{
    public function payment(OrderInterface $payments);
    public function typePayment();
}