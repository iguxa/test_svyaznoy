<?php
namespace Services\PaymentStrategy;
use Contracts\OrderInterface;
use Services\PaymentStrategy\Contacts\PaymentsStrategy;

abstract class Payments
{
    private $payments;

    public function __construct(PaymentsStrategy $payments)
    {
        $this->payments = $payments;
    }
    public function payment(OrderInterface $order)
    {
        $this->payments->payment($order);
        $order->paid();
    }
    public function getType()
    {
        return $this->payments->typePayment();
    }
}