<?php

namespace Services\PaymentStrategy\PaymentsType;

use Contracts\OrderInterface;
use Controllers\StoreController;
use Services\PaymentStrategy\Contacts\PaymentsStrategy;
use Services\PaymentStrategy\Payments;

class PickUpPayment implements PaymentsStrategy
{

    public function payment(OrderInterface $order)
    {
        return $this->processPayment($order);
    }

    protected function processPayment(OrderInterface $order)
    {
        return 'Оплата при получении';

    }

    public function typePayment()
    {
        return 'Самовывоз';
    }
}