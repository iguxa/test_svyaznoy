<?php

namespace Services\PaymentStrategy\PaymentsType;

use Contracts\OrderInterface;
use Controllers\StoreController;
use Services\PaymentStrategy\Contacts\PaymentsStrategy;
use Services\PaymentStrategy\Payments;

class OnlinePayment implements PaymentsStrategy
{

    public function payment(OrderInterface $order)
    {
        return $this->processPayment($order);
    }

    protected function processPayment(OrderInterface $order)
    {
        if ($this->isValidPayment()) {
            $store = new StoreController();
            foreach ($order->getCart()->getItems() as $item){
                $store->removeItem($item);
            }
        }else{
            throw new \Exception('Не прошла оплата');
        }

        return 'Доставка оформлена';
    }
    protected function isValidPayment(): bool
    {
        return true;
    }

    public function typePayment()
    {
        return 'Онлайн оплата';
    }
}