<?php
namespace Controllers;
use Contracts\CartInterface;
use Contracts\ItemInterface;
use Services\Traits\Singleton;

class CartController implements CartInterface
{
    use Singleton;
    protected $items = [];

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function addItemToCart(ItemInterface $item)
    {
        return $this->addItem($item);
    }
    protected function addItem(ItemInterface $item)
    {
        if (!$item->inStock()) {
            throw new \Exception('В заказе есть товары которых нет в наличии');
        }else{
            (new StoreController())->holdItem($item);
            array_push($this->items, $item);
        }

        return $this;
    }
}