<?php


namespace Controllers;


use Contracts\CartInterface;
use Contracts\OrderInterface;

class OrderController implements OrderInterface
{
    protected $cart;
    protected $paidStatus;
    protected $cancel = false;
    /**
     * @var bool
     */
    private $canceled;
    /**
     * @var array
     */
    private $events;

    public function __construct(CartInterface $cart)
    {
        $this->cart = $cart;
    }
    /**
     * @return CartInterface
     */
    public function getCart(): CartInterface
    {
        return $this->cart;
    }

    public function paid()
    {
        $this->paidStatus = true;
    }
    /**
     * @return mixed
     */
    public function isPaid()
    {
        return $this->paidStatus;
    }

    public function checkTotalStatus()
    {
        if(!$this->paidStatus){
            $this->cancel();
            $this->dispatch(new CanceledOrder($this));
        }
    }

    private function cancel()
    {
        $this->canceled = true;
    }

    private function dispatch($event)
    {
        $this->events[] = $event;
    }

    public function flushEvents()
    {
        $events = $this->events;
        $this->events = [];
        return $events;
    }

    public function isCanceled()
    {
        return $this->canceled;
    }
}