<?php

namespace Controllers;

use Contracts\StoreInterface;

class StoreController implements StoreInterface
{
    public function removeItem(\Contracts\ItemInterface $item)
    {
        /*удаляем товар на складе и тп логика */
        return $this;
    }

    public function holdItem(\Contracts\ItemInterface $item)
    {
        /*резервируем товар на складе и тп логика */
        return $this;
    }
}