<?php

namespace Models;

use Contracts\ItemInterface;

class Item implements ItemInterface
{
    protected $quantity;
    protected $name;
    protected $specs;

    public function __construct(string $name,int $quantity, array $specs = [])
    {
        $this->quantity = $quantity;
        $this->name = $name;
        $this->specs = $specs;
    }

    /**
     * @return array
     */
    public function getSpecs(): array
    {
        return $this->specs;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function inStock(): bool
    {
        return $this->quantity ? true : false;
    }


}