<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
require_once 'vendor/autoload.php';

use Controllers\{OrderController , PaymentsController ,CartController};
use Models\User;
use Models\Item;
use Services\PaymentStrategy\PaymentsType\{OnlinePayment,PickUpPayment};

$user = new User();

$item = new Item('Телефон',2);
$cartInstance = CartController::single();
$cartInstance->addItemToCart($item);

$item2 = new Item('Телефизор',1);
$cartInstance = CartController::single();
$cartInstance->addItemToCart($item2);

$order = new OrderController(CartController::single());

$payment = new PaymentsController(new OnlinePayment());
$payment->payment($order);
$payment = new PaymentsController(new PickUpPayment());
$payment->payment($order);
$order->isPaid();
