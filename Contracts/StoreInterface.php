<?php


namespace Contracts;


interface StoreInterface
{
    public function removeItem(ItemInterface $item);
    public function holdItem(ItemInterface $item);
}