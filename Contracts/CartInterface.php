<?php


namespace Contracts;


interface CartInterface
{
    public function addItemToCart(ItemInterface $item);
    public function getItems();
}