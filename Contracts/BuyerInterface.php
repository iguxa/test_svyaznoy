<?php
namespace Contracts;

interface BuyerInterface
{
    public function addItem(ItemInterface $item,CartInterface $cart);
    public function checkoutCart(CartInterface $cart);
    public function delivery(CartInterface $cart);
}