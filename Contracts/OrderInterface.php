<?php


namespace Contracts;


interface OrderInterface
{
    public function __construct(CartInterface $cart);
}