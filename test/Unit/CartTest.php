<?php

use Controllers\CartController;
use Controllers\OrderController;
use Controllers\PaymentsController;
use Models\Item;
use PHPUnit\Framework\TestCase;
use Services\PaymentStrategy\PaymentsType\OnlinePayment;

class CartTest extends TestCase
{
    public function testAdd()
    {
        $item = new Item('Телефон',2);
        $cartInstance = CartController::single();
        $cartInstance->addItemToCart($item);
        $find = false;
        foreach ($cartInstance->getItems() as $item){
            if($item->getName() == 'Телефон'){
                $find = true;
            }
        };
        $this->assertTrue($find);
    }

    public function testCanceledIfNotPaid()
    {
        $item = new Item('Телефизор',1);
        $cartInstance = CartController::single();
        $cartInstance->addItemToCart($item);
        $order = new OrderController(CartController::single());
        $order->checkTotalStatus();
        $this->assertTrue($order->isCanceled());
    }

    public function testNotCanceledIfPaid()
    {
        $item = new Item('Телефизор',1);
        $cartInstance = CartController::single();
        $cartInstance->addItemToCart($item);
        $order = new OrderController(CartController::single());
        $payment = new PaymentsController(new OnlinePayment());
        $payment->payment($order);
        $order->checkTotalStatus();
        $this->assertFalse($order->isCanceled());
    }

    public function testPaidStatus()
    {
        $item = new Item('Телефизор',1);
        $cartInstance = CartController::single();
        $cartInstance->addItemToCart($item);
        $order = new OrderController(CartController::single());
        $payment = new PaymentsController(new OnlinePayment());
        $payment->payment($order);
        $order->checkTotalStatus();
        $this->assertTrue($order->isPaid());
    }

}