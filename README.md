Необходимо спроектировать и реализовать бизнес-логику компонента «Обработка заказа в интернет-магазине». Бизнес-требования к компоненту описаны ниже.

Интервьюер играет роль бизнес-заказчика и может ответить на любые вопросы по требованиям. Интервьюер также играет роль «Архитектора» и может ответить на любые вопросы, относительно того, как работает система, в которой будет работать компонент.

Бизнес-требования:<br>
    1. Покупатель должен иметь возможность добавлять товар в корзину<br>
    2. Покупатель должен иметь возможность оформить заказ на товары из корзины<br>
    3. Заказ может быть оплачен онлайн либо при получении в пункте самовывоза, если онлайн-оплата не поступила – заказ отменяется, покупатель получает оповещение<br>
    4. Если товар есть на складе – покупатель должен быть оповещен о том, что заказ готов, если товара нет на складе – покупатель должен быть оповещен о том, что товара, нет а заказ – отменен<br>

Требования по реализации: работоспособность всех описанных бизнес-требований должна проверяться тестами. 

Основное внимание стоит уделить реализации бизнес-логики, детали инфраструктурного слоя можно оставить в стороне.
